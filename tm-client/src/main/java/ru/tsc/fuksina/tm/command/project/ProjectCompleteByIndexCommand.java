package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete project by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(getToken(), index, Status.COMPLETED));
    }
}
