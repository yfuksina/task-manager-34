package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findOneByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @Nullable
    User findOneByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
