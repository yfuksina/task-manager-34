package ru.tsc.fuksina.tm.exception.system;

import ru.tsc.fuksina.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
