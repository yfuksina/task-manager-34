package ru.tsc.fuksina.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest(@Nullable final String token) {
        super(token);
    }

}
